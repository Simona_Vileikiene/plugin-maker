<?php

namespace App;

use App;
use Request;
use Illuminate\Database\Eloquent\Model;

class OrdersProduct extends Model
{
    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['order_id', 'product_id', 'name', 'price', 'qty'];

    //RALATIONSHIPS. each order product has one order
    public function order()
    {
        return $this->belongsTo('App\Order');
    }

    //RALATIONSHIPS. each order product has one product
    public function product()
    {
        return $this->belongsTo('App\Product');
    }
}