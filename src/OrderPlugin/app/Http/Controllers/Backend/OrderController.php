<?php
namespace App\Http\Controllers\Backend;

use App\Http\Requests\Frontend\OrderRequest;
use App\Order;
use App\OrderHelper;
use App\OrdersProduct;
use Request;
use DB;

use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    //admin. list
    public function index()
    {
        $items = Order::whereNotNull('id');
        if (!empty(Request::input('search'))) {
            $items = $items->where(function ($query) {
                $query->where('name', 'like', '%'.Request::input('name').'%')
                    ->orwhere('email', 'like', '%'.Request::input('email').'%');
            });
        }
        $items = $items->orderby("created_at", 'DESC')->paginate(50);
        return view('backend/order/index', compact("items"));
    }

    //admin. edit
    public function edit($id)
    {
        $item = Order::findorFail($id);
        return view('backend/order/edit', compact('item'));
    }

    //admin. save edit
    public function update($id, Request $request)
    {
        $item = Order::findorFail($id);
        $item->update($request->all());
        $this->manageProducts($item, $request);
        $orderHelper = new OrderHelper();
        $orderHelper->updateTotal($item);
        $orderHelper->changeStatus($item->id, $request->input('status'));
        return redirect()->route('order.index');
    }

    //admin. destroy
    public function destroy($id)
    {
        $item = Order::findorFail($id);
        $item->products()->delete();
        $item->delete();
        return redirect()->route('order.index');
    }

    //no used, but laravel should has it for resource route
    public function invoice($id)
    {
        $item = Order::findorFail($id);
        $pdf = \App::make('dompdf.wrapper');
        $pdf->loadHTML(view('frontend/order/invoice', compact('item')));
        return $pdf->stream();
    }

    private function manageProducts($item, $request)
    {
        //update
        if(is_array($request->input('products_name'))) {
            $prices = $request->input('products_price');
            $qty = $request->input('products_qty');
            foreach ($request->input('products_name') as $id => $name) {
                if (!empty($name) && isset($prices[$id]) && isset($qty[$id])) {
                    $product = OrdersProduct::find($id);
                    $product->update(['name' => $name, 'price' => $prices[$id], 'qty' => $qty[$id]]);
                }
            }
        }
        //remove
        if(!empty($request->input('products_del'))) {
            $item->products()->whereIn('id', $request->input('products_del'))->delete();
        }
        //store new
        if(is_array($request->input('products_name_new'))) {
            $prices = $request->input('products_price_new');
            $qty = $request->input('products_qty_new');
            foreach ($request->input('products_name_new') as $id => $name) {
                if (!empty($name) && isset($prices[$id]) && isset($qty[$id])) {
                    $item->products()->save(OrdersProduct::create(['name' => $name, 'price' => $prices[$id], 'qty' => $qty[$id]]));

                }
            }
        }
    }
}
