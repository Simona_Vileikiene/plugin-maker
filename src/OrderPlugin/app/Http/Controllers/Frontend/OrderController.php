<?php

namespace App\Http\Controllers\Frontend;

use App\Notifications\Purchase;
use App\OrderHelper;
use App\OrdersSetting;
use Artme\Paysera\Facades\Paysera;
use \Cart;
use Illuminate\Http\Request;

use App\Category;
use App\Order;
use App\OrdersProduct;
use App\Product;

use App\Http\Requests\Frontend\OrderRequest;

class OrderController extends \App\Http\Controllers\Controller
{
    /**
     * Prepare common data for view
     *
     * @return array
     */
    public function getPageData() {
        $indexController = new IndexController();
        $indexController->data = $this->data;
        return $indexController->getPageData();
    }

    //order page after cart
    public function index($page=null)
    {
        $this->data['cartDeliveryId'] = Session('cart_delivery_id');
        return view('frontend/order/index', $this->getPageData());
    }

    /**
     ** PAYMENT
     */
    //save order from cart - cart
    public function orderCartStore(OrderRequest $request)
    {
        $order = Order::create($request->all());
        foreach (Cart::content() as $item) {
            $product = new OrdersProduct([
                'product_id' => $item->id,
                'name' => $item->name,
                'price' => $item->price,
                'qty' => $item->qty
            ]);
            $order->products()->save($product);
        }
        $orderHelper = new OrderHelper();
        $orderHelper->finishOrder($request, $order);
        Cart::destroy();
        return redirect()->back()->with('orderMessage', 'Order OK');
    }

    //payment success - paysera
    public function orderSuccess()
    {
        Cart::destroy();
        return redirect(Category::find(Order::siteCategoryId())->link)->with('orderMessage', 'Order OK');
    }

    //payment cancel - paysera
    public function orderCancel()
    {
        return redirect(Category::find(Order::siteCategoryId())->link)->with('orderError', 'Order error');
    }

    //paysera form - paysera
    public function payseraCallback(Request $request)
    {
        $data = Paysera::parsePayment($request);
        if(isset($data['orderid'])) {
            Order::changeStatus($data['orderid'], 2);
            echo "OK";
        }
    }
}
