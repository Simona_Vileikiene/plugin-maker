@component('mail::layout')
{{-- Header --}}
@slot('header')
@component('mail::header', ['url' => config('app.url')])
    <img src="{{ config('app.url') }}/img/logo.png" alt="Turtinga moteris">
@endcomponent
@endslot

{!! $body !!}

<b>Užsakymo duomenys:</b><br />
{{ $order->name }}<br />
El. paštas: {{ $order->email }}<br />
@if(!empty($order->phone))
    Telefonas: {{ $order->phone }}<br />
@endif
<br />

<b>Prekės</b>
@component('mail::table')
| Prekė         | Kiekis        |  Kaina     |
| ------------- |:-------------:| :---------:|
@foreach($order->products as $one)
    | {{ $one->name.' '.$one->combination_name  }} | {{ $one->qty  }} | {{ $one->price  }} Eur
@endforeach
    | {{ $order->delivery_name  }}                 | 1                | {{ $order->delivery_price }} Eur
@endcomponent

@component('mail::panel')
Viso mokėti: {{ $order->total }} Eur
@endcomponent

{{-- Footer --}}
@slot('footer')
@component('mail::footer')
    <b>{{ trans('db.pageName') }}</b><br />
    Telefonas: {{ trans('db.pagePhone') }} |
    El. paštas: {{ trans('db.pageEmail') }}
@endcomponent
@endslot

@endcomponent
