<hr/>
<h3>{{ __('Products') }}</h3>
@if(isset($item))
    @foreach($item->products as $one)
        <div class="row mb-2">
            <div class="col col-sm-4 col-12">
                {!! Form::label('', __('Ticket name').'*', ['class'=>'control-label']) !!}
                {!! Form::text('products_name['.$one->id.']', $one->name, ['class'=>'form-control ']) !!}
            </div>
            <div class="col col-sm-4 col-12">
                {!! Form::label('', __('Ticket price').'*', ['class'=>'control-label']) !!}
                {!! Form::text('products_price['.$one->id.']', $one->price, ['class'=>'form-control ']) !!}
            </div>
            <div class="col col-sm-2 col-12">
                {!! Form::label('', __('Ticket qty').'*', ['class'=>'control-label']) !!}
                {!! Form::text('products_qty['.$one->id.']', $one->qty, ['class'=>'form-control ']) !!}
            </div>
            <div class="col col-sm-2 pt-4">
                {!! Form::checkbox('products_del[]', $one->id, FALSE) !!} <small>{{ __('Remove') }}</small>

            </div>
        </div>
    @endforeach
@endif
<div class="row mb-2" data-child>
    <div class="col col-sm-4 col-12">
        {!! Form::label('', __('New ticket name').'*', ['class'=>'control-label']) !!}
        {!! Form::text('products_name_new[]', '', ['class'=>'form-control ']) !!}
    </div>
    <div class="col col-sm-4 col-12">
        {!! Form::label('', __('New ticket price').'*', ['class'=>'control-label']) !!}
        {!! Form::text('products_price_new[]', '', ['class'=>'form-control ']) !!}
    </div>
    <div class="col col-sm-2 col-12">
        {!! Form::label('', __('New ticket price').'*', ['class'=>'control-label']) !!}
        {!! Form::text('products_qty_new[]', '', ['class'=>'form-control ']) !!}
    </div>
</div>
<div class="text-right">
    <span class="btn btn-sm btn-outline-success" data-child-add><i class="fas fa-plus"></i> {{ __('Create new') }}</span>
</div>
