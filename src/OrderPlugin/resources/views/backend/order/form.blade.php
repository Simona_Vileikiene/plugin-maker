{!! Form::label('Statusas', '', ['class'=>'control-label']) !!}<br/>
{!! Form::select('status', \App\SeminarsOrder::STATUSES, @$statuse, ['class'=>'form-control']) !!}<br/>

@include('frontend/order/form')

{!! Form::hidden('delivery_id', (isset($item))?$item->delivery_id:0, ['class'=>'form-control']) !!}
{!! Form::label('Pristatymo pavadinimas', '', ['class'=>'control-label']) !!}<br/>
{!! Form::text('delivery_name', @$delivery_name, ['class'=>'form-control']) !!}<br/>
{!! Form::label('Pristatymo kaina', '', ['class'=>'control-label']) !!}
{!! Form::text('delivery_price', @$delivery_price, ['class'=>'form-control']) !!}<br/>

{!! Form::hidden('payment_id', (isset($item))?$item->payment_id:0, ['class'=>'form-control']) !!}
{!! Form::label('Apmokėjimo pavadinimas', '', ['class'=>'control-label']) !!}
{!! Form::text('payment_name', @$payment_name, ['class'=>'form-control']) !!}<br/>

@include('backend.order.products')

{!! Form::submit(__('Submit'), ['class'=>'btn btn-outline-main btn-lg']) !!}