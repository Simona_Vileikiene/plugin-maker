@extends('backend/layout')

@section('content')

    <h1 class="card-header">{{ __('Edit') }}</h1>

    <div class="card-body">
        {!! Form::model($item, ['method' => 'PATCH', 'route' => ['order.update', $item->id], 'files' => true]) !!}
        @include('backend.order.form')
        {!! Form::close() !!}
    </div>

@endsection
