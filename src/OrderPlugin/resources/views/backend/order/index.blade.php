@extends('backend/layout')

@section('content')


    <div class="row">
        <h1 class="col-lg-2 text-lg-left text-center">{{ __('Order') }}</h1>
        <div class="col-lg-8 pt-2">
            {!! Form::open(array('method' => 'get', 'route' => 'order.index', 'class' => 'row')) !!}
            <div class="col-md-8">
                {!! Form::text('search', Request::input('search'), ['class'=>'form-control', 'placeholder'=>'Vardas, El. paštas, Prekė']) !!}
            </div>
            <div class="col-md-4 text-left">
                {!! Form::submit('Search', ['class'=>'btn btn-info']) !!}
                @if(Request::input('search')!='')
                    <a href="{{ route("order.index") }}" class="btn btn-default">Išvalyti</a>
                @endif
            </div>
            {!! Form::close() !!}
        </div>
        <div class="col col-lg-2 pt-2 text-right">
            <a href="{{ route("orderssetting.index") }}" class="btn btn-outline-primary">
                <i class="fas fa-tools"></i> {{ __('Order settings') }}
            </a>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover">
            <tr>
                <th>#</th>
                <th>Statusas</th>
                <th>Vardas</th>
                <th>El. paštas</th>
                <th>Telefonas</th>
                <th>Suma</th>
                <th>Data</th>
                <th>Prekės</th>
                @if(App\OrdersSetting::where('type', 1)->count()>0)
                    <th>Pristatymas</th>
                @endif
                @if(App\OrdersSetting::where('type', 2)->count()>0)
                    <th>Apmokėjimas</th>
                @endif
                <th width="150"></th>
            </tr>
            @foreach($items as $item)

                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ @App\SeminarsOrder::STATUSES[$item->status] }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->phone }}</td>
                    <td>{{ $item->total }} Eur</td>
                    <td>{{ $item->created_at->format('Y-m-d') }}</td>
                    <td>
                        @if(count($item->products)>0)
                            <details>
                                <summary>Prekės</summary>
                                <table class="table">
                                    @foreach($item->products as $one)
                                        <tr>
                                            <td>{{ $one->name.' '.$one->combination_name  }}</td>
                                            <td>{{ $one->qty  }}</td>
                                            <td>{{ $one->price  }} Eur</td>
                                        </tr>
                                    @endforeach

                                </table>
                            </details>
                        @endif
                    </td>
                    @if(App\OrdersSetting::where('type', 1)->count()>0)
                        <td>{{ $item->delivery_name }} ({{ $item->delivery_price }})</td>
                    @endif
                    @if(App\OrdersSetting::where('type', 2)->count()>0)
                        <td>{{ $item->payment_name }}</td>
                    @endif
                    <td>
{{--                        <a href="{{ action("OrderController@invoice", $item->id) }}" class="btn btn-warning"--}}
{{--                           title="Sąskaita" target="_blank"><i class="glyphicon glyphicon-list-alt"></i></a>--}}
                        <a href="{{ route("order.edit", $item->id) }}" class="btn btn-outline-info btn-sm"
                           title="{{ __('Edit') }}"><i class="far fa-edit"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'route' => ['order.destroy', $item->id], 'class'=>'d-inline',
                        'onclick'=>"return confirm('".__('Ar you sure?')."')", 'title'=>__('Remove')]) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-outline-danger btn-sm']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>

            @endforeach
        </table>
    </div>

    {!! $items->appends(['search' => Request::input('search')])->render() !!}
@endsection
