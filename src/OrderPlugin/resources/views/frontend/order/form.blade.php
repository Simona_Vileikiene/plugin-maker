<div class="mb-3">
    {!! Form::label(__('Name').'*', '', ['class'=>'control-label']) !!}
    {!! Form::text('name', @$name, ['required' => '', 'class'=>'form-control '.(($errors->has('name'))?'is-invalid':'')]) !!}
    @error('name')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

<div class="mb-3">
    {!! Form::label(__('Email').'*', '', ['class'=>'control-label']) !!}
    {!! Form::text('email', @$email, ['required' => '', 'class'=>'form-control '.(($errors->has('email'))?'is-invalid':'')]) !!}
    @error('email')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

<div class="mb-3">
    {!! Form::label(__('Phone').'*', '', ['class'=>'control-label']) !!}
    {!! Form::text('phone', @$phone, ['required' => '', 'class'=>'form-control '.(($errors->has('phone'))?'is-invalid':'')]) !!}
    @error('phone')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>