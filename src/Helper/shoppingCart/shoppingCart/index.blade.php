@extends('frontend.layout')

@section('content')
    <h1 class="h1">{{ $page->name  }}</h1>

    @if(count($items)>0)
        @if (Session::get('cartMaxError'))
            <p class="alert alert-danger">{{ trans('db.cartMaxError').Session::get('cartMaxError') }}</p>
        @endif

        <div id="ajax-cart-view">
            <table class="table table-hover">
                <tr class="d-sm-table-row d-none">
                    <th class="d-md-table-cell d-none">{{ __('Photo') }}</th>
                    <th>{{ __('Title') }}</th>
                    <th>{{ __('Price') }}</th>
                    <th>{{ __('Qty') }}</th>
                    <th>{{ __('Total') }}</th>
                </tr>
                @foreach($items as $id=>$item)
                    <?php $product = App\Product::find($item->id); $product = $product->translate(); ?>
                    @if($product)
                        <tr>
                            <td class="thumbnail d-md-table-cell d-none">{!! $product->img  !!}</td>
                            <td><a href="{{ $product->link }}" class="title">{{ $product->name  }}

                                    {{ $item->options->combination['name'] }}</a></td>
                            <td><small class="d-sm-none d-inline">{{ __('Price') }}:</small> {{ $item->price  }} Eur</td>
                            <td>
                                {!! Form::text('qty-'.$item->id, $item->qty, ['class'=>'form-control d-inline-block']) !!}
                                <a href="{{ action("Frontend\ShoppingCartController@update", $id) }}"
                                   data-name="shoppingCartUpdate"
                                   data-id="{{ $item->id }}"
                                   class="alert-success ajax-cart-update btn btn-sm">
                                    <i class="far fa-save"></i></a>
                                <a href="{{ action("Frontend\ShoppingCartController@destroy", $id) }}"
                                   class="alert-danger ajax-cart-destroy btn btn-sm"
                                   onclick="return confirm('{{ trans('admin.remove_confirm') }}')"><i
                                            class="far fa-trash-alt"></i></a>
                            </td>
                            <td class="ajax-cart-subtotal-{{ $id }}"><span>{{ $item->subtotal  }}</span> Eur</td>
                        </tr>
                    @endif
                @endforeach
            </table>

            <table class="table table-hover">
                @if($deliverMethods && $deliverMethods->count()>0  && Cart::total()>0)
                    <tr>
                        <td></td>
                        <td class="text-right" style="font-weight: bold">Viso:</td>
                        <td><span data-name="ajax-cart-subtotal">{{ Cart::total() }}</span> Eur</td>
                    </tr>
                    <tr>
                        <td></td>
                        <td class="text-right" style="font-weight: bold">Pristatymas:</td>
                        <td><span data-name="ajax-cart-delivery">0.00</span> Eur</td>
                    </tr>
                @endif
                <tr>
                    <td></td>
                    <td class="text-right" style="font-weight: bold">Viso mokėti:</td>
                    <td style="font-weight: bold"><span data-name="ajax-cart-total">{{ Cart::total() }}</span> Eur</td>
                </tr>
            </table>
            <br/>

            @if($deliverMethods && $deliverMethods->count()>0  && Cart::total()>0)
                {!! Form::open(array('action' => ['Frontend\ShoppingCartController@chooseDelivery'], 'files' => true)) !!}
                {!! Form::label('Pristatymas', '', ['class'=>'control-label']) !!}<br/>
                @foreach($deliverMethods->get() as $item)
                    {!! Form::radio('delivery_id', $item->id, @$delivery_id,
                    ['class'=>'radio', 'data-delivery-price'=>$item->price, 'id' => 'delivery_id-'.$item->id]) !!}
                    {!! Form::label('delivery_id-'.$item->id, $item->name.' ('.$item->price.' Eur') !!}<br/>
                @endforeach
                <br/>
                {!! Form::submit('Toliau', ['class'=>'btn btn-info btn-lg']) !!}
                {!! Form::close() !!}
            @else
                <a href="{{ $orderPage->link }}" class="btn btn-info btn-lg">Toliau</a>
            @endif

        </div>
    @else
        <div class="alert alert-danger"> Krepšelis tuščias</div>
    @endif
@endsection

@section('footer')
    <script>
        function deliveryPrice() {
            var delivery = $('[data-delivery-price]:checked').attr('data-delivery-price')*1;
            $('[data-name="ajax-cart-delivery"]').text(delivery.toFixed(2));
            var subtotal = $('[data-name="ajax-cart-subtotal"]').text()*1;
            var total = delivery+subtotal;
            $('[data-name="ajax-cart-total"]').text(total.toFixed(2));
        }
        $('[data-delivery-price]').click(function () {
            deliveryPrice();
        });
        $('[data-name="shoppingCartUpdate"]').click(function () {
            var id = $(this).attr('data-id');
            var qty2 = $('input[name="qty-' + id + '"]').val();
            var link = $(this).attr('href') + '?qty=' + qty2;
            location.href = link;
            return false;
        });
    </script>
@endsection
