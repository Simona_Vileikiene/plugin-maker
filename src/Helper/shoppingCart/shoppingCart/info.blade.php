<a href="{{ $shoppingCartPage->link }}">{{ $shoppingCartPage->title }}</a>
<span data-cart-info-count>{{ Cart::count() }}</span>
<b><span data-cart-info-total>{{ Cart::total() }}</span> Eur</b>

<div class="alert alert-success alert-dismissible core-alert fade show" data-cart-message role="alert"
     @if (!Session::get('cartMessage') ) style="display: none" @endif >
    {{ __('Congratulations, your selected product has been successfully added to') }} <a href="{{ $shoppingCartPage->link }}">{{ __('cart') }}</a>.
    <a class="close" data-hide-closest=".alert">×</a>
</div>

@if (Session::get('cartError'))
    <p class="alert alert-danger">{{ Session::get('cartError') }}</p>
@endif
