<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Requests\OrderDeliveryRequest;
use App\OrdersSetting;
use \Cart;
use Request;

use App\Category;
use App\Product;
use App\ProductsCombination;

class ShoppingCartController extends \App\Http\Controllers\Controller
{

    /**
     * Prepare common data for view
     *
     * @return array
     */
    public function getPageData() {
        $indexController = new IndexController();
        $indexController->data = $this->data;
        return $indexController->getPageData();
    }

    public function index()
    {
        $this->data['items'] = Cart::content();
        $this->data['total'] = Cart::total();
        $this->data['deliverMethods'] = false;
        if(class_exists('\App\OrdersSetting')) {
            $this->data['deliverMethods'] = App\OrdersSetting::where('type', 1);
        }
        $this->data['orderPage'] = Category::find(13);
        return view('frontend/shopingcart/index', $this->getPageData());
    }

    //add product to cart
    public function add($productId = null, $qty =0)
    {
        $item = Product::find($productId);
        if($item) {
            Cart::add($item->id, $item->name, $qty, $item->priceTrue);
            return redirect()->back()->with('cartMessage', 'Cart OK');
        }
        return redirect()->back()->with('cartError', 'Cart error');
    }

    //user. update cart
    public function update($id)
    {
        $subtotal = 0;
        if(Cart::get($id) and is_numeric(Request::input('qty'))) {
            $cartItem = Cart::get($id);
            $qty = Request::input('qty');
            Cart::update($id, $qty);
        }
        return redirect()->back();
    }

    //user. delete from cart
    public function destroy($id)
    {
        if(Cart::get($id)) {
            Cart::remove($id);
        }
        return redirect()->back();
    }


    //save delivery from cart
    public function chooseDelivery(OrderDeliveryRequest $request)
    {
        $request->session()->put('cart_delivery_id', $request->input("delivery_id"));
        return redirect(action('Frontend\OrderController@index'));
    }

    //get product's combination qty in cart
    private function getCartQtyByCombinationId($productId, $combinationId) {
        foreach (Cart::content() as $id => $cartItem) {
            if($cartItem->id == $productId) {
                return ['id' => $id, 'qty' => $cartItem->qty];
            }
        }
        return null;
    }

    public function getInfo() {
        return ['count' => Cart::count(), 'total' => Cart::total()];
    }
}
