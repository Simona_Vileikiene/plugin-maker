<?php

use Illuminate\Database\Seeder;

class OrdersSettingPluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::table('orders_settings')->insert([
            'id' => 1,
            'type' => 0,
            'info' => 'Pardavėjo rekvizitai',
            'name' => 'Pardavėjo rekvizitai',
            'body' => '<p><b>Beenet.lt</b></p><p>El. paštas: info@beenet.lt</p>',
        ]);
        DB::table('orders_settings')->insert([
            'id' => 2,
            'type' => 0,
            'info' => 'Sąskaitos serija',
            'name' => 'BNT'
        ]);
        DB::table('orders_settings')->insert([
            'id' => 3,
            'type' => 1,
            'name' => 'Pristatymas',
            'price' => 2
        ]);
        DB::table('orders_settings')->insert([
            'id' => 4,
            'type' => 2,
            'name' => 'Sąskaita'
        ]);
        DB::table('orders_settings')->insert([
            'id' => 5,
            'type' => 2,
            'name' => 'Paysera'
        ]);

        DB::table('orders_settings')->insert([
            'id' => 6,
            'type' => 3,
            'key' => 'Laiškas užsisakius',
            'name' => 'Laiškas užsisakius',
            'body' => 'Sveiki, Jūsų užsakymas gautas.'
        ]);
        DB::table('orders_settings')->insert([
            'id' => 7,
            'type' => 3,
            'key' => 'Laiškas apmokėjus',
            'name' => 'Laiškas apmokėjus',
            'body' => 'Jūsų užsakymo apmokėjimas gautas'
        ]);
    }
}
