frontend Controller
$this->data['homeBanners'] = App\Banner::orderBy('priority', 'ASC')->get();

home page
@include('frontend.partials.banner')

https://cdnjs.com/libraries/OwlCarousel2

css
.home-slide {
    .container {
        position: relative;
        height: 100%;
    }
    article {
        height: 500px;
        text-align: center;
        background-position: center;
        background-size: cover; //100% auto;

        .vertical-center {
            z-index: 10;
        }
    }
    h3 {
        font-weight: 600;
        font-style: normal;
        color: $color-black;
        font-size: 54px;
        line-height: 1;
        text-transform: uppercase;
    }
    .btn {
        margin-top: 20px;
        padding: 10px 20px;
        font-size: 18px;
    }
}


banner.blade.php
<div id="carouselExampleIndicators" class="carousel home-slide" data-ride="carousel">
    <div class="carousel-inner" role="listbox">
        <div class="bg"></div>
        @foreach($homeBanners as $k=>$item)
            <article class="{{ ($k==0)?'active':'' }} {{ ($item->mini==1)?'mini':'' }} carousel-item"
                     style="background-image: url('{!! $item->imgLink !!}')">
                <div class="container">
                    <div class="vertical-center">
                        <h3>{{ $item->translate()->name }}</h3>
                        <p>{{ strip_tags($item->body) }}</p>
                        @if(!empty($item->link)) <a href="{{ $item->link }}"
                                                    class="btn btn-main">{{ __('More') }}</a> @endif
                    </div>
                </div>
            </article>
        @endforeach
    </div>

    @if(count($homeBanners) > 1)
        <ol class="carousel-indicators">
            @foreach($homeBanners as $k=>$item)
                <li data-target="#carouselExampleIndicators" data-slide-to="{{ $k }}"
                    class="@if($k==0) active @endif"></li>
            @endforeach
        </ol>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    @endif
</div>
