<?php

namespace Beenet\PluginMaker\Console;

use Illuminate\Console\Command;

class PluginMaker extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:plugin {name} {type}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Make plugin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $pluginType = $this->argument('type');
        if (!in_array($pluginType, ['sile', 'order', 'letter'])) {
            $this->error("Wrong plugin type - " . $pluginType);
        }
        $pluginName = $this->argument('name');
        $pluginPath = base_path('vendor/beenet/plugin-maker/src/' . $this->getPluginPathType($pluginType));
        $files = $this->getDirContents($pluginPath);
        $this->copyFiles($files, TRUE, $pluginType, $pluginName);
        $installed = $this->copyFiles($files, FALSE, $pluginType, $pluginName);
        if ($installed) {
            $this->addRouter($pluginName, $pluginType);
            $this->addMenu($pluginName);
            $this->info("Plugin $pluginName made successfully");
        } else {
            $this->error("Plugin $pluginName is already installed");
        }
    }

    private function getPluginPathType($pluginType)
    {
        switch ($pluginType) {
            case 'order':
                $pluginPathType = 'OrderPlugin';
                break;
            case 'letter':
                $pluginPathType = 'LetterPlugin';
                break;
            default:
                $pluginPathType = 'Plugin';
        }
        return $pluginPathType;
    }

    private function getDirContents($dir, &$results = array())
    {
        $files = scandir($dir);
        foreach ($files as $key => $value) {
            $path = realpath($dir . DIRECTORY_SEPARATOR . $value);
            if (!is_dir($path)) {
                if (strpos($value, '.txt') === false) {
                    $results[] = ['name' => $value, 'path' => $path, 'type' => 1];
                }
            } else if ($value != "." && $value != "..") {
                $this->getDirContents($path, $results);
                $results[] = ['name' => $value, 'path' => $path, 'type' => 0];
            }

        }
        return $results;
    }

    private function copyFiles($files, $onlyCategories = FALSE, $pluginType = 'sile', $pluginName = 'Order')
    {
        $module = $this->argument('name');
        foreach ($files as $file) {
            $pluginPath = 'vendor\beenet\plugin-maker\src\\' . $this->getPluginPathType($pluginType) . '\\';
            $pathInfo = (string)str_replace($pluginPath, "", $file['path']);
            if ($pluginType == 'order' && $pluginName != 'Order') {
                $pathInfo = (string)str_replace(['Order', 'order'], [ucfirst($module), strtolower($module)], $pathInfo);
            } elseif ($pluginType == 'sile') {
                $pathInfo = (string)str_replace(['Sile', 'sile'], [ucfirst($module), strtolower($module)], $pathInfo);
            }
            if ($onlyCategories && $file['type'] == 0) {
                if (!file_exists($pathInfo)) {
                    mkdir($pathInfo);
                }
            }
            if (!$onlyCategories && $file['type'] == 1) {
                if (!file_exists($pathInfo)) {
                    copy($file['path'], $pathInfo);
                    $str = file_get_contents($pathInfo);
                    if ($pluginType == 'order' && $pluginName != 'Order') {
                        $str = str_replace(['Order', 'order'], [ucfirst($module), strtolower($module)], $str);
                    } elseif ($pluginType == 'sile') {
                        $str = str_replace(['Sile', 'sile'], [ucfirst($module), strtolower($module)], $str);
                    }
                    file_put_contents($pathInfo, $str);
                } else {
                    break;
                    return FALSE;
                }
            }
        }
        return TRUE;
    }

    private function addRouter($module, $pluginType = 'sile')
    {
        $pathInfo = base_path('routes/admin.php');
        $str = file_get_contents($pathInfo);
        if ($pluginType == 'order') {
            $router = "Route::resource('order', 'OrderController')->except('show'); //beenet";
            if ($module != 'Order') {
                $router = str_replace(['Order', 'order'], [ucfirst($module), strtolower($module)], $router);
            }
        } elseif ($pluginType == 'letter') {
            $router = "Route::resource('letter', 'LetterController')->except('show'); 
            Route::resource('lettersSubscriber', 'LettersSubscriberController')->except('show'); //beenet";
        } else {
            $router = "Route::resource('sile', 'SileController')->except('show'); //beenet";
            $router = str_replace(['Sile', 'sile'], [ucfirst($module), strtolower($module)], $router);
        }
        $str = str_replace('//beenet', $router, $str);
        file_put_contents($pathInfo, $str);

        $pathInfo = base_path('routes/web.php');
        $str = file_get_contents($pathInfo);
        if ($pluginType == 'order') {
            $router = "Route::post('orderCartStore', 'OrderController@orderCartStore')->name('order.cartStore');
            Route::get('payseraCallback', 'OrderController@payseraCallback')->name('artme.paysera.callback');
            Route::get('orderSuccess', 'OrderController@orderSuccess')->name('order.success');
            Route::get('orderCancel', 'OrderController@orderCancel')->name('order.cancel');
            Route::get('/order/invoice/{id}', 'OrderController@invoice')->name('order.invoice'); //beenet";
            if ($module != 'Order') {
                $router = str_replace(['Order', 'order'], [ucfirst($module), strtolower($module)], $router);
            }
        } elseif ($pluginType == 'letter') {
            $router = "Route::get('api/letter/send', 'LetterController@sendLetter');
            Route::get('api/letter/open/{any}/{any2}', 'LetterController@openLetter');
            Route::get('unsubscribe/{any}', 'LetterController@unsubscribe');
            Route::post('form/subscribe', 'LetterController@subscribe'); //beenet";
        }
        $str = str_replace('//beenet', $router, $str);
        file_put_contents($pathInfo, $str);
    }

    private function addMenu($module)
    {
        $pathInfo = base_path('resources/views/backend/partials/menu.blade.php');
        $str = file_get_contents($pathInfo);
        $router = "<div class=\"col col-lg-4 col-md-6 nav-link\">
    <div class=\"card\">
        <div class=\"card-body\">
            <h5 class=\"card-title\">
                <a href=\"{{ route('sile.index') }}\"
                   class=\"@if (Request::is('admin/sile*')) current @endif\">
                    <i class=\"fas fa-puzzle-piece\"></i> {{ __('Sile') }}
                </a>
            </h5>
            <p class=\"card-text\">{{ __('Total') }}: {{ App\Sile::count() }}</p>
            <p class=\"mb-0\"><a href=\"{{ route('sile.index') }}\" class=\"btn btn-outline-main\">{{ __('View') }}</a></p>
        </div>
    </div>
</div>";
        $router = str_replace(['Sile', 'sile'], [ucfirst($module), strtolower($module)], $router);
        $str = str_replace($str, $str . $router, $str);
        file_put_contents($pathInfo, $str);
    }
}
