@extends('frontend.layout')

@section('content')

	<article >
		<h1>{{ $pageItem->name }}</h1>
		@include('frontend.partials.breadcumb')
		{{ $pageItem->data }}
		<figure class="col-md-3 thumbnail">{!! $pageItem->img !!}</figure>

		{!! $pageItem->body !!}
	</article>

@endsection 