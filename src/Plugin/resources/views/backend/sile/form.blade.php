{!! Form::label('', __('Photo'), ['class'=>'control-label']) !!}
@if(isset($item) and $item->img!="")
    <span style="width:100px;display:inline-block" class="thumb">{!! $item->img !!}</span>
@endif
{!! Form::file('photo', ['class'=>'form-control']) !!}<br />

<div class="mb-3">
    {!! Form::label('', __('Title').'*', ['class'=>'control-label']) !!}
    {!! Form::text('name', @$name, ['required' => '', 'class'=>'form-control '.(($errors->has('name'))?'is-invalid':'')]) !!}
    @error('name')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

<div class="mb-3">
    {!! Form::label('', __('Link').'*', ['class'=>'control-label', 'data-comment'=>' '.__('(Blank - created automatically by name; Unique)')]) !!}
    {!! Form::text('slug', @$slug, ['class'=>'form-control multi '.(($errors->has('slug'))?'is-invalid':'')]) !!}
    @error('slug')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

{!! Form::label('', __('Body'), ['class'=>'control-label']) !!}
<span class="multi" name="body">{!! Form::textarea('body', @$body, ['class'=>'form-control redactor']) !!}</span><br />

@if(count($categories)>2)
    {!! Form::label('', __('Category').'*', ['class'=>'control-label']) !!}
    {!! Form::select('category_id', $categories, @$category_id, ['class'=>'form-control']) !!}
    <br/>
@else
    {!! Form::hidden('category_id', @$category_id, ['class'=>'form-control']) !!}
@endif

{!! Form::submit(__('Submit'), ['class'=>'btn btn-outline-main btn-lg']) !!}
