@extends('backend/layoutForm')

@section('contentForm')

    <h1 class="card-header">{{ __('Edit') }}</h1>

    <div class="card-body">
        {!! Form::model($item, ['method' => 'PATCH', 'route' => ['sile.update', $item->id], 'files' => true]) !!}
        @include('backend/sile/form')
        {!! Form::close() !!}
    </div>

@endsection
