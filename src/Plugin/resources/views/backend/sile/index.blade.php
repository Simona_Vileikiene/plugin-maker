@extends('backend/layout')

@section('content')

    <div class="row">
        <h1 class="col-lg-9 text-lg-left text-center">{{ __('Sile') }}</h1>
        <div class="col col-lg-3 text-right p-2">
            <a href="{{ route("sile.create") }}" class="btn btn-outline-success">
                <i class="fas fa-plus"></i> {{ __('Create new') }}
            </a>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover actions-3" @if(Request::input('sortable')) data-sortable="\App\Sile" @endif>
            <tr @if(Request::input('sortable')) class="sortable-disable" @endif>
                <th>#</th>
                <th>{{ __('Photo') }}</th>
                <th>{{ __('Title') }}</th>
                <th>{{ __('Link') }}</th>
                @if(count($categories)>2)
                    <th>{{ __('Category') }}</th>
                @endif
                <th width="150">
                    <a href="{{ route("sile.index") }}{{ (Request::input('sortable'))?'':'?sortable=1' }}"
                       class="btn btn-sm btn-outline-warning text-nowrap"
                       title="{{ (Request::input('sortable'))?__('Stop'):__('Change priority')}}">
                        <i class="fas fa-sort"></i> {{ (Request::input('sortable'))?__('Stop'):__('Change priority') }}
                    </a>
                </th>
            </tr>
            @foreach($items as $item)
                <tr data-sortable-id="{{ $item->id }}">
                    <td>{{ $item->id }}</td>
                    <td>{!! $item->img !!}</td>
                    <td>{{ $item->name }}</td>
                    <td><a href="{{ url($item->link) }}"
                           target="_blank">{{ url($item->link) }}</a></td>
                    @if(count($categories)>2)
                        <td>{{ ($item->category)?$item->category->name:'' }}</td>
                    @endif
                    <td>
                        <a href="{{ route("sile.edit", $item->id) }}" class="btn btn-outline-info btn-sm"
                           title="{{ __('Edit') }}"><i class="far fa-edit"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'route' => ['sile.destroy', $item->id], 'class'=>'d-inline',
                        'onclick'=>"return confirm('".__('Are you sure?')."')", 'title'=>__('Remove')]) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-outline-danger btn-sm']) !!}
                        {!! Form::close() !!}

                        @if(Request::input('sortable'))
                            <span class="btn btn-sm"><i class="fas fa-arrows-alt"></i></span>
                        @endif
                    </td>
                </tr>

            @endforeach
        </table>
    </div>
    {!! $items->appends(['search' => Request::input('search')])->render() !!}
@endsection
