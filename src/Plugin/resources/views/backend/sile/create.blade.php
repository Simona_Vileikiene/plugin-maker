@extends('backend/layoutForm')

@section('contentForm')

    <h1 class="card-header">{{ __('Create new') }}</h1>

    <div class="card-body">
        {!! Form::open(array('route' => ['sile.store'], 'files' => true)) !!}
        @include('backend/sile/form')
        {!! Form::close() !!}
    </div>

@endsection
