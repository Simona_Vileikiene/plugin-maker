<?php

use Illuminate\Database\Seeder;

class SilePluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'id' => DB::table('categories')->max('id')+1,
            'name' => 'Sile kats.',
            'slug' => 'sile',
            'plugin' => 'sile'
        ]);
    }
}
