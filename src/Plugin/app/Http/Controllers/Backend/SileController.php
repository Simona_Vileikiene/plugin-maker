<?php

namespace App\Http\Controllers\Backend;

use App\CategoryHelper;
use App\Priority;
use App\TranHelper;
use Request;
use App\Sile;
use App\Tran;
use App\Image;
use Illuminate\Validation\Validator;
use App\Http\Requests\Backend\SileRequest;
use Response;
use App\Http\Controllers\Controller;

use Illuminate\Support\Str;

class SileController extends Controller 
{
    public function __construct()
    {
        $this->tranHelper = new TranHelper();
        $this->categoryHelper = new CategoryHelper();
        $this->categories = $this->categoryHelper->getAllSubcategoriesList(Sile::siteCategoryId());
    }

    //admin. list
    public function index()
    {
        if(Request::input('sortable')) {
            $items = Sile::orderby("priority", 'DESC')->paginate(9999);
        } else {
            $items = Sile::orderby("priority", 'DESC')->paginate(50);
        }
        $categories = $this->categories;
       	return view('backend/sile/index', compact("items", 'categories'));
    }

    //admin. create new
    public function create($id=0)
    {
        $categories = $this->categories;
        return view('backend/sile/create', compact('id', 'categories'));
    }

    //admin. save create
    public function store(SileRequest $request)
    {
        $item = Sile::create($request->all());
        $item->update(["slug" => Str::slug($request->input("slug"))]);
        $priority = new Priority(Sile::whereNotNull('id'), $item);
        $priority->addPriority();
        //upload new image
        if($request->file('photo')) {
            $imageName = Image::uploadImage($item, $request->file('photo'), Sile::IMAGES_PATH);
            $item->photo = $imageName;
            $item->save();
        }
        //if multi lang save values
        $this->tranHelper->setItemTranslations($item->id, Request::segment(2), $request);
        return redirect()->route('sile.index')->with(['message' => __('Successfully created')]);
    }

    //admin. edit
    public function edit($id)
    {
        $item = Sile::findorFail($id);
        $categories = $this->categories;
        return view('backend/sile/edit', compact('item', 'categories'));
    }

    //admin. save edit
    public function update($id, SileRequest $request)
    {
        $item = Sile::findorFail($id);
        $item->update($request->all());
        $item->update(["slug" => Str::slug($request->input("slug"))]);
        //delete old and upload new image
        if($request->file('photo')) {
            Image::removeImage($item->photo, Sile::IMAGES_PATH);
            $imageName = Image::uploadImage($item, $request->file('photo'), Sile::IMAGES_PATH);
            $item->photo = $imageName;
            $item->save();
        }
        //if multi lang save values
        $this->tranHelper->setItemTranslations($item->id, Request::segment(2), $request);
        return redirect()->route('sile.index')->with(['message' => __('Successfully updated')]);
    }

    //admin. destroy
    public function destroy($id)
    {
        $item = Sile::findorFail($id);
        Image::removeImage($item->photo, Sile::IMAGES_PATH);
        $this->tranHelper->removeItemTranslations($item->id, Request::segment(2));
        $item->delete();
        //check weight
        $priority = new Priority(Sile::whereNotNull('id'));
        $priority->fixPriorities();
        return redirect()->route('sile.index')->with(['message' => __('Successfully deleted')]);
    }
}
