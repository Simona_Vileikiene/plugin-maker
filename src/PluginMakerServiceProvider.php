<?php

namespace Beenet\PluginMaker;

use Illuminate\Support\ServiceProvider;
use Beenet\PluginMaker\Console\PluginMaker;

class PluginMakerServiceProvider extends ServiceProvider
{

    /**
     * Register the application services.
     *
     *
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('command.plugin-maker', function ($app) {
            return new PluginMaker();
        });
        $this->commands('command.plugin-maker');
    }
}
