<?php

use Illuminate\Database\Seeder;

class LetterPluginSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('settings')->insert([
            'key' => 'letterSubscribe',
            'name' => 'Naujienlaiškis. Žinutė užsiprenumeravus',
            'body' => 'Ačiū. Jūs įtrauktas į sąrašą.',
        ]);
        DB::table('settings')->insert([
            'key' => 'letterUnsubscribe',
            'name' => 'Naujienlaiškis. Žinutė atsisakius',
            'body' => 'Ačiū. Jūs pašalintas iš sąrašo.',
        ]);
    }
}
