<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLettersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('letters', function (Blueprint $table) {
            $table->increments('id');

            $table->string('name');
            $table->text('body')->nullable();
            $table->date('send_data')->nullable();
            $table->time('send_time')->nullable();
            $table->integer('subscribers_total')->nullable();
            $table->integer('opened_total')->nullable();
            $table->smallInteger('sended')->nullable();

            $table->timestamps();
        });

        Schema::create('letters_subscribers', function (Blueprint $table) {
            $table->increments('id');

            $table->string('email');
            $table->string('name')->nullable();
            $table->timestamp('unsubscribed')->nullable();
            $table->integer('sending_letter_id')->nullable();

            $table->timestamps();
        });

        Schema::create('letters_opens', function (Blueprint $table) {
            $table->increments('id');

            $table->string('email')->nullable();
            $table->integer('letter_id')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('letters');
        Schema::drop('letters_subscribers');
        Schema::drop('letters_opens');
    }
}
