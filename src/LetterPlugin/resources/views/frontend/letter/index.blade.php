@extends('site.layout')

@section('content')

    <div class="page">

        <h1>{{ $page->name }}</h1>
        @include('frontend.partials.breadcumb')

        @if (Session::has('message'))
            <p class="alert alert-success">{{ Session::get('message') }}</p>
        @else
            @include('frontend/letter/form')
        @endif

    </div>

@endsection