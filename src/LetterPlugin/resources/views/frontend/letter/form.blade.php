{!! Form::open(array('action' => ['Frontend\LetterController@subscribe'], 'files' => true)) !!}

<div class="mb-3">
    {!! Form::label(__('Name').'*', '', ['class'=>'control-label']) !!}
    {!! Form::text('name', @$name, ['required' => '', 'class'=>'form-control '.(($errors->has('name'))?'is-invalid':'')]) !!}
    @error('name')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

<div class="mb-3">
    {!! Form::label(__('Email').'*', '', ['class'=>'control-label']) !!}
    {!! Form::text('email', @$email, ['required' => '', 'class'=>'form-control '.(($errors->has('email'))?'is-invalid':'')]) !!}
    @error('email')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

<div>
    {!! Form::hidden('g-recaptcha-response', '', ['id' => 'captcha-field-newsletter']) !!}
    <span class="captcha"></span>
</div>

{!! Form::submit(__('Submit'), ['class'=>'btn btn-primary btn-main']) !!}
{!! Form::close() !!}