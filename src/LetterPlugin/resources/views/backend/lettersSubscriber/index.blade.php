@extends('admin/layout')

@section('content')

    <div class="row">
        <h1 class="col-lg-9 text-lg-left text-center">{{ __('Letters subscribers') }}</h1>
        <div class="col col-lg-3 text-right p-2">
            <a href="{{ route("lettersSubscriber.create") }}" class="btn btn-outline-success">
                <i class="fas fa-plus"></i> {{ __('Create new') }}
            </a>
        </div>
    </div>

    <div class="table-responsive">
        <table class="table table-hover actions-2">
            <tr>
                <th>#</th>
                <th>Vardas</th>
                <th>El. paštas</th>
                <th>Atsisakęs</th>
                <th width="200"></th>
            </tr>
            @foreach($items as $item)

                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->email }}</td>
                    <td>{{ $item->unsubscribed }}</td>
                    <td>
                        <a href="{{ route("lettersSubscriber.edit", $item->id) }}" class="btn btn-outline-info btn-sm"
                           title="{{ __('Edit') }}"><i class="far fa-edit"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'route' => ['lettersSubscriber.destroy', $item->id], 'class'=>'d-inline',
                         'onclick'=>"return confirm('".__('Ar you sure?')."')", 'title'=>__('Remove')]) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-outline-danger btn-sm']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>

            @endforeach
        </table>
    </div>

    {!! $items->render() !!}
@endsection 