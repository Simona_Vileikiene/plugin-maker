@extends('admin.layout')

@section('content')

	<h1 class="card-header">{{ __('Edit') }}</h1>

	<div class="card-body">
		{!! Form::model($item, ['method' => 'PATCH', 'route' => ['lettersSubscriber.update', $item->id], 'files' => true]) !!}
			@include('backend/lettersSubscriber/form')
        {!! Form::close() !!}
	</div>
	
@endsection