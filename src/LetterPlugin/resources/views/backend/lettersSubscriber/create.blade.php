@extends('admin/layout')

@section('content')

	<h1 class="card-header">{{ __('Create new') }}</h1>

	<div class="card-body">
		{!! Form::open(array('route' => ['lettersSubscriber.store'], 'files' => true)) !!}
			@include('backend/lettersSubscriber/form')
		{!! Form::close() !!}
	</div>
	
@endsection