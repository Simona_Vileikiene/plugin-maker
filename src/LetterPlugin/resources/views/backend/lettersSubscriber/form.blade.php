<div class="mb-3">
    {!! Form::checkbox('unsubscribed_check', 1, @$unsubscribed_check, ['class'=>'form-control radio']) !!}
    {!! Form::label('Atsisakęs naujienų', '', ['class'=>'control-label']) !!}
</div>

<div class="mb-3">
    {!! Form::label('', __('Name').'*', ['class'=>'control-label']) !!}
    {!! Form::text('name', @$name, ['required' => '', 'class'=>'form-control '.(($errors->has('name'))?'is-invalid':'')]) !!}
    @error('name')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

<div class="mb-3">
    {!! Form::label(__('Email').'*', '', ['class'=>'control-label']) !!}
    {!! Form::text('email', @$email, ['required' => '', 'class'=>'form-control '.(($errors->has('email'))?'is-invalid':'')]) !!}
    @error('email')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

{!! Form::submit(__('Submit'), ['class'=>'btn btn-outline-main btn-lg']) !!}
