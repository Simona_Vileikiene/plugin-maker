<div class="mb-3">
    {!! Form::label('', __('Letter subject').'*', ['class'=>'control-label']) !!}
    {!! Form::text('name', @$name, ['required' => '', 'class'=>'form-control '.(($errors->has('name'))?'is-invalid':'')]) !!}
    @error('name')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

<div class="mb-3">
    {!! Form::label('Siuntimo data', '', ['class'=>'control-label',
    'data-comment'=>' pvz. 2015-11-21; nieko neįrašius - siųsti pradės dabar']) !!}
    {!! Form::text('send_data', @$send_data, ['required' => '', 'class'=>'form-control datepicker '.(($errors->has('send_data'))?'is-invalid':'')]) !!}<br/>
    @error('send_data')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

<div class="mb-3">
    {!! Form::label('Siuntimo laikas', '', ['class'=>'control-label', 'data-comment'=>' pvz. 13:45:00']) !!}
    {!! Form::text('send_time', @$send_time, ['required' => '', 'class'=>'form-control '.(($errors->has('send_time'))?'is-invalid':'') ]) !!}<br/>
    @error('send_time')
    <span class="invalid-feedback" role="alert"><strong>{{ $message }}</strong></span>
    @enderror
</div>

<div class="mb-3">
    {!! Form::label('Laiškas', '', ['class'=>'control-label']) !!}
    <span class="multi"
          name="body">{!! Form::textarea('body', @$body, ['class'=>'form-control redactor']) !!}</span><br/>
</div>

{!! Form::submit(__('Submit'), ['class'=>'btn btn-outline-main btn-lg']) !!}
