@extends('admin/layout')

@section('content')

    <div class="row">
        <h1 class="col-lg-2 text-lg-left text-center">{{ __('Letter') }}</h1>
        <div class="col col-lg-4 pt-2 text-right">
            <a href="{{ route("lettersSubscriber.index") }}" class="btn btn-outline-primary">
                <i class="fas fa-tools"></i> {{ __('Letters subscribers') }}
            </a>
            <a href="{{ route("letter.create") }}" class="btn btn-outline-success ml-3">
                <i class="fas fa-plus"></i> {{ __('Create new') }}
            </a>
        </div>
    </div>


    <div class="table-responsive">
        <table class="table table-hover actions-2">
            <tr>
                <th>#</th>
                <th>Laiško tema</th>
                <th>Siuntimo laikas</th>
                <th>Gavėjų skaičius</th>
                <th>Atidarymo procentas</th>
                <th width="200"></th>
            </tr>
            @foreach($items as $item)

                <tr>
                    <td>{{ $item->id }}</td>
                    <td>{{ $item->name }}</td>
                    <td>{{ $item->send_data }} {{ $item->send_time }}</td>
                    @if(!empty($item->sended))
                        <td>{{ $item->subscribers_total }}</td>
                        <td>{{ $item->openRate }}% ({{ $item->subscribers_total.'/'.$item->opened_total }})</td>
                    @elseif($item->subscribers_total)
                        <td colspan="2">Siunčiama... {{ $item->sendRate }}%</td>
                    @else
                        <td colspan="2">Dar neišsiųsta</td>
                    @endif
                    <td>
                        <a href="{{ route("letter.edit", $item->id) }}" class="btn btn-outline-info btn-sm"
                           title="{{ __('Edit') }}"><i class="far fa-edit"></i></a>
                        {!! Form::model($item, ['method' => 'DELETE', 'route' => ['letter.destroy', $item->id], 'class'=>'d-inline',
                        'onclick'=>"return confirm('".__('Ar you sure?')."')", 'title'=>__('Remove')]) !!}
                        {!! Form::submit(' X ', ['class'=>'btn btn-outline-danger btn-sm']) !!}
                        {!! Form::close() !!}
                    </td>
                </tr>

            @endforeach
        </table>
    </div>

    {!! $items->render() !!}
@endsection 