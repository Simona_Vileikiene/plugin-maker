@extends('admin/layout')

@section('content')

	<h1 class="card-header">{{ __('Create new') }}</h1>

	<div class="card-body">
		{!! Form::open(array('route' => ['letter.store'], 'files' => true)) !!}
			@include('backend/letter/form')
		{!! Form::close() !!}
	</div>
	
@endsection