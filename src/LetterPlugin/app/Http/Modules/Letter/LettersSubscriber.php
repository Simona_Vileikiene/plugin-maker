<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LettersSubscriber extends Model
{
    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['name', 'email', 'unsubscribed', 'sending_letter_id'];

    //get link attribute [$item->link]
    protected function getUnsubscribedCheckAttribute()
    {
        if(empty($this->unsubscribed)) {
            return 0;
        }
        return 1;
    }

}