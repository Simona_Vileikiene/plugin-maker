<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class LettersOpen extends Model
{
    //no guarded, cause multi lang has unnecessary fields
    protected $fillable = ['letter_id', 'email'];
}