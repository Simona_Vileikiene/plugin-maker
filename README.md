# Laravel plugin maker

Laravel 7,8

Laravel plugin is a software component that adds a specific feature to an existing app.
This package makes new controller, model, request & views for backend and frontend.
Creating a new plugin only takes one command (make:plugin) and you will end up with the following file structure:
```bash
.
└── app
    └── Http
        └── Controllers
            └── Backend
                ├── <Plugin>Controller.php
        └── Modules
            ├── <Plugin>.php
        └── Requests
            └── Backend
                ├── <Plugin>Request.php
└── database
    └── migrations
        ├── create_<Plugin>_table.php
    └── seeds
        ├── <Plugin>Seeder.php
└── resources
    └── views
        └── backend
            └── <plugin>
                ├── create.blade.php
                ├── edit.blade.php
                ├── form.blade.php
                ├── index.blade.php
        └── frontend
            └── <plugin>
                ├── index.blade.php
                ├── view.blade.php
```

## Installation
First require package with composer:
```sh
$ composer require beenet/plugin-maker
$ composer dump-autoload
```
Add service provider to config/app.php:
```php
'providers' => [
    ...
    Beenet\PluginMaker\PluginMakerServiceProvider::class,
],
```
Plugin types: sile, order, letter
## Use
Install module:
```sh
$ php artisan make:plugin [pluginName] [type]
$ composer dump-autoload
$ php artisan route:cache
$ php artisan migrate
$ php artisan db:seed --class=<Plugin>PluginSeeder
```